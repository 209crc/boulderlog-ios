//
//  Route.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/24.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import SwiftyJSON

let RouteImagesMax = 5

class Route: NSObject {

    var id = 0
    var spot_id = 0
    var user_id = 0
    var name = ""
    var grade = GradeManagerGradeUndefined
    var image_url1 = ""
    var image_url2 = ""
    var image_url3 = ""
    var image_url4 = ""
    var image_url5 = ""
    var created_at: Double = 0
    var updated_at: Double = 0
    var removed_at: Double = 0

    // MARK: Constructors
    override init() {
        
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        spot_id = json["spot_id"].intValue
        name = json["name"].stringValue
        grade = json["grade"].intValue
        image_url1 = json["image_url1"].stringValue
        image_url2 = json["image_url2"].stringValue
        image_url3 = json["image_url3"].stringValue
        image_url4 = json["image_url4"].stringValue
        image_url5 = json["image_url5"].stringValue
        created_at = Utils.unixTimeFrom(railsDateString: json["created_at"].stringValue)
        updated_at = Utils.unixTimeFrom(railsDateString: json["updated_at"].stringValue)
        removed_at = Utils.unixTimeFrom(railsDateString: json["removed_at"].stringValue)
    }

    func gradeString() -> String {
        return GradeManager.gradeString(grade: self.grade)
    }
    
    func numberOfImageUrls() -> Int {
        // 前詰めでURLが入っている前提
        if self.image_url1 == "" {
            return 0
        } else if self.image_url2 == "" {
            return 1
        } else if self.image_url3 == "" {
            return 2
        } else if self.image_url4 == "" {
            return 3
        } else if self.image_url5 == "" {
            return 4
        } else {
            return 5
        }
    }
    
    func imageUrlStringOfIndex(index: Int) -> String {
        switch index {
        case 0:
            return self.image_url1
        case 1:
            return self.image_url2
        case 2:
            return self.image_url3
        case 3:
            return self.image_url4
        case 4:
            return self.image_url5
        default:
            return ""
        }
    }
}
