//
//  User.swift
//  BoulderLog
//
//  Created by 209crc on 2018/11/30.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    var id = 0
    var fId = ""
    var tId = ""
    var lId = ""
    var name = ""
    var email = ""  // 基本的にAPIレスポンスには含まれない

    // MARK: Constructors
    override init() {
        
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        fId = json["fId"].stringValue
        tId = json["tId"].stringValue
        lId = json["lId"].stringValue
        name = json["name"].stringValue
        email = json["email"].stringValue
    }

}
