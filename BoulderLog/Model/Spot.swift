//
//  Spot.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/17.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import SwiftyJSON

// memo : 都道府県コードは、北海道が1

class Spot: NSObject {
    
    var id = 0
    var name = ""
    var prefectureId = PrefectureIdUndefined
    var address = ""
    var authorized = false
    var created_at: Double = 0
    var updated_at: Double = 0
    
    // MARK: Constructors
    override init() {
        
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        prefectureId = json["prefecture_id"].intValue
        address = json["address"].stringValue
        authorized = json["authorized"].boolValue
    }    
}
