//
//  APIError.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/18.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import SwiftyJSON

let SPIErrorCodeUndefined = 0
let SPIErrorStatusUndefined = 0

class APIError {
    
    // MARK: Properties
    var code = 0
    var status = 0
    var message = ""
    
    // MARK: Constructor
    init() {
    }
    
    init(string: String) {
        message = string
    }
    
    init(json: JSON) {
        code = json["error_code"].intValue
        status = json["status"].intValue
        message = json["message"].stringValue
    }
    
    init(error: Error) {
        message = error.localizedDescription
    }
}
