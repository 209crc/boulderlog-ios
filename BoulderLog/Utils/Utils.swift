//
//  Utils.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/01.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

let UnixTimeUndefined: Double = 0

class Utils {
    static func unixTimeFrom(railsDateString: String) -> Double {
        if railsDateString == "" {
            return UnixTimeUndefined
        }
        
        return Utils.dateFrom(railsDateString: railsDateString).timeIntervalSince1970
    }
    
    static func dateFrom(railsDateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        let date = dateFormatter.date(from: railsDateString)
        if (date == nil) {
            return Date(timeIntervalSince1970: UnixTimeUndefined)
        }
        return date!
    }
    
    static func dateYmd(unixTime: Double) -> String {
        if unixTime == UnixTimeUndefined {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Tokyo")
        
        let date = Date(timeIntervalSince1970: unixTime)
        return dateFormatter.string(from: date)
    }
    
    static func isEqual(float1: CGFloat, float2: CGFloat) -> Bool {
        if fabs(float1 - float2) < 0.0001 {
            return true
        }
        return false
    }
    
    static func showAlert(context: UIViewController, title: String, message: String) {
        self.showAlert(context: context, title: title, message: message, actionTitle: "OK")
    }
    
    static func showAlert(context: UIViewController, title: String, message: String, actionTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: actionTitle, style: .default) { (action) -> Void in
        }

        alert.addAction(action)
        
        context.present(alert, animated: true, completion: nil)

    }
}
