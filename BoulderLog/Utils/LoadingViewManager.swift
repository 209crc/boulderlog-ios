//
//  LoadingViewManager.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/24.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class LoadingViewManager {
    static var shared = LoadingViewManager()
    
    var loadingView: UIView
    var indicator: UIActivityIndicatorView
    
    // MARK: Constructor
    init() {
        loadingView = UIView()
        loadingView.frame = (UIApplication.shared.keyWindow?.frame)!
        loadingView.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.5)
        
        indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.center = loadingView.center
        loadingView.addSubview(indicator)
    }
    
    func show() {
        let window = UIApplication.shared.keyWindow
        window?.addSubview(loadingView)
        indicator.startAnimating()
    }
    
    func dismiss() {
        loadingView.removeFromSuperview()
        indicator.stopAnimating()
    }

}
