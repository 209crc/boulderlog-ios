//
//  APICaller.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/17.
//  Copyright © 2018年 209crc. All rights reserved.
//

// todo : ATS設定をちゃんとする

import UIKit
import Alamofire
import SwiftyJSON

class APICaller {
    static var shared = APICaller()
    
    // MARK: Constructor
    init() {
        
    }
    
    // todo : header設定
    private static func headerDefaultValues() -> HTTPHeaders {
        // todo 本番 / デバッグ切り替え
        
        return [
            "Accespt": "application/json",
            "Content-Type": "application/json"
        ]
    }
    
    // MARK: Error
    func onError(error: Error) {
        self.onError(errorString: error.localizedDescription)
    }
    
    func onError(errorJson: JSON) {
        if errorJson["message"].exists() {
            self.onError(errorString: errorJson["message"].stringValue)
        } else {
            self.onError(errorString: "内部エラー")
        }
    }
    
    func onError(errorString: String) {
        let alert: UIAlertController = UIAlertController(title: "エラー", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancelAction)
        
        var baseViewController = UIApplication.shared.keyWindow?.rootViewController
        while baseViewController?.presentedViewController != nil {
            baseViewController = baseViewController?.presentedViewController
        }
        baseViewController?.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Users
    func postUsersLogin(user: User, successHandler: @escaping (User)->(), errorHandler: @escaping (APIError)->()) {
        let urlString = HOST_URL + "/api/users/login"
        
        // todo : fId以外も作る
        let params: Parameters = [
            "name" : user.name,
            "fId" : user.fId,
            "email" : user.email,
            ]
        
        Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: APICaller.headerDefaultValues()).responseJSON {(response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    if json["ok"].boolValue {
                        let user = User(json: json["user"])
                        
                        successHandler(user)
                        
                    }
                }
                
            case .failure(let error):
                errorHandler(APIError(error: error))
            }
        }
    }
    
    // MARK: Spots
    func getSpots(successHandler: @escaping ([Spot])->(), errorHandler: @escaping (APIError)->()) {
        let urlString = HOST_URL + "/api/spots"
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: APICaller.headerDefaultValues()).responseJSON {(response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    // todo : json ok確認
                    var result = [Spot]()
//                    for item in json["spots"].arrayValue {
                    for item in json.arrayValue {
                        let spot = Spot(json: item)
                        result.append(spot)
                    }
                    successHandler(result)

                }
                
            case .failure(let error):
                errorHandler(APIError(error: error))
            }
        }
    }
    
    func getRoutes(spotId: Int, successHandler: @escaping ([Route])->(), errorHandler: @escaping (APIError)->()) {
        if spotId < 0 {
            onError(errorString: "内部エラー")
            errorHandler(APIError.init())
            return
        }
        
        guard let urlString = (HOST_URL + "/api/routes/\(spotId)").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            errorHandler(APIError.init())
            return
        }
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: APICaller.headerDefaultValues()).responseJSON {(response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    
                    // todo : json ok確認
                    var result = [Route]()
//                    for item in json["spots"].arrayValue {
                    for item in json.arrayValue {
                        let route = Route(json: item)
                        result.append(route)
                    }
                    successHandler(result)
                    
                }
                
            case .failure(let error):
                errorHandler(APIError(error: error))
            }
        }
    }
    
    // MARK: Spot
    func postSpotProvisionallyRegister(name: String, prefectureCode: Int, successHandler: @escaping ()->(), errorHandler: @escaping (APIError)->()) {
        let urlString = HOST_URL + "/api/spot/provisionallyRegister"
        
        let params: Parameters = [
            "name" : name,
            "prefectureId" : prefectureCode,
        ]
        
        Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: APICaller.headerDefaultValues()).responseJSON {(response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    if json["ok"].boolValue {
                        successHandler()
                    } else {
                        self.onError(errorJson: json)
                        errorHandler(APIError(json: json))
                    }
                } else {
                    self.onError(errorString: "エラー")
                    errorHandler(APIError.init(string: "エラー"))
                }
            case .failure(let error):
                errorHandler(APIError(error: error))
            }
        }
    }

    // MARK: Routes
    // todo : 画像も送る
    func postRouteCreate(spotId: Int, userId: Int, name: String, grade: Int, successHandler: @escaping ()->(), errorHandler: @escaping (APIError)->()) {
        let urlString = HOST_URL + "/api/routes/create"
        
        let params: Parameters = [
            "spot_id" : spotId,
            "user_id" : userId,
            "name" : name,
            "grade" : grade,
            ]
        
        Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: APICaller.headerDefaultValues()).responseJSON {(response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    if json["ok"].boolValue {
                        successHandler()
                    } else {
                        self.onError(errorJson: json)
                        errorHandler(APIError(json: json))
                    }
                } else {
                    self.onError(errorString: "エラー")
                    errorHandler(APIError.init(string: "エラー"))
                }
            case .failure(let error):
                errorHandler(APIError(error: error))
            }
        }
    }
}
