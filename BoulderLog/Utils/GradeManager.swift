//
//  GradeManager.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/10.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

let GradeManagerGradeUndefined = -1
let GradeManagerGradeMin = 0

class GradeManager {
    // https://docs.google.com/spreadsheets/d/1zb7W9-o19XdxrBv0VXquVYuvIJwJFmCDmpoOinYPhAs/edit#gid=0
    static func gradeString(grade: Int) -> String {
        switch grade {
        case 0..<20:
            return "7級以下"
        case 20..<30:
            return "6級"
        case 30..<40:
            return "5級"
        case 40..<50:
            return "4級"
        case 50..<60:
            return "3級"
        case 60..<70:
            return "2級"
        case 70..<80:
            return "1級"
        case 80..<90:
            return "1段"
        case 90..<100:
            return "2段"
        case 100..<110:
            return "3段"
        case 110..<120:
            return "4段"
        case 120..<130:
            return "5段"
        case 130..<140:
            return "6段"
        case 140..<150:
            return "7段以上"
        default:
            return ""
        }
    }
    
    static func gradeString(index: Int) -> String {
        switch index {
        case 0:
            return "7級以下"
        case 1:
            return "6級"
        case 2:
            return "5級"
        case 3:
            return "4級"
        case 4:
            return "3級"
        case 5:
            return "2級"
        case 6:
            return "1級"
        case 7:
            return "1段"
        case 8:
            return "2段"
        case 9:
            return "3段"
        case 10:
            return "4段"
        case 11:
            return "5段"
        case 12:
            return "6段"
        case 13:
            return "7段以上"
        default:
            return ""
        }
    }
    
    static func gradeValue(index: Int) -> Int {
        switch index {
        case 0:
            // 7級以下
            return 10
        case 1:
            // 6級
            return 20
        case 2:
            // 5級
            return 30
        case 3:
            // 4級
            return 40
        case 4:
            // 3級
            return 50
        case 5:
            // 2級
            return 60
        case 6:
            // 1級
            return 70
        case 7:
            // 1段
            return 80
        case 8:
            // 2段
            return 90
        case 9:
            // 3段
            return 100
        case 10:
            // 4段
            return 110
        case 11:
            // 5段
            return 120
        case 12:
            // 6段
            return 130
        case 13:
            // 7段以上
            return 141
        default:
            return 0
        }
    }

    static func numberOfGrades() -> Int {
        return 14
    }
}
