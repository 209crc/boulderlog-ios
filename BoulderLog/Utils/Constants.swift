//
//  Constants.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/24.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

// todo : テスト環境とローカル環境

#if RELEASE_BUILD
    // todo : 本番環境
    let HOST_URL = "http://localhost:3000"
#else
    let HOST_URL = "http://localhost:3000"
#endif

class ColorConst {
    class func Primary() -> UIColor {
        return UIColor.init(hex: "1B3CFF")
    }

    func PrimaryLight() -> UIColor {
        return UIColor.init(hex: "83b9ff")
    }

    func PrimaryDark() -> UIColor {
        return UIColor.init(hex: "005ecb")
    }

    func Secondary() -> UIColor {
        return UIColor.init(hex: "66bb6a")
    }
    
    func SecondaryLight() -> UIColor {
        return UIColor.init(hex: "98ee99")
    }
    
    func SecondaryDark() -> UIColor {
        return UIColor.init(hex: "338a3e")
    }
    
    func Caution() -> UIColor {
        return UIColor.init(hex: "E77FBE")
    }

}
