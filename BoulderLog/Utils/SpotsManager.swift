//
//  SpotsManager.swift
//  BoulderLog
//
//  Created by 209crc on 2018/05/31.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

let PrefectureIdUndefined = 48
let PrefecturesCount = 48   // その他を含める

// todo : 名前を変える(spotsとprefecture)
class SpotsManager {

    static var shared = SpotsManager()
    
    private var spots = [Spot]()
    var prefeturesSpotsMap = [[Spot]](repeating: [Spot](), count: PrefecturesCount)
    
    // MARK: Constructor
    init() {
        
    }

    // MARK: Methods
    func setSpots(spotsArr: [Spot]){
        self.spots = spotsArr
        self.prefeturesSpotsMap = [[Spot]](repeating: [Spot](), count: PrefecturesCount)
        
        for spot in spots {
            if (spot.prefectureId - 1 >= 0) {   // データがおかしい場合のために一応チェック(落ちるので)
                prefeturesSpotsMap[spot.prefectureId - 1].append(spot)
            }
        }
    }
    
    func isSpotsSetted() -> Bool {
        if spots.count > 0 {
            return true
        }
        return false
    }
    
    func getPrefectureList() -> Array<String> {
        return ["北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県",
                "茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県",
                "新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県",
                "静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県",
                "奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県",
                "徳島県", "香川県", "愛媛県", "高知県", "福岡県", "佐賀県", "長崎県",
                "熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県", "その他"];
    }
    
    func getPrefectureCount() -> Int {
        return PrefecturesCount;
    }
    
    func getSpots(prefectureCode: Int) -> [Spot] {
        return prefeturesSpotsMap[prefectureCode]
    }

    func getSpot(prefectureCode: Int, index: Int) -> Spot {
        return prefeturesSpotsMap[prefectureCode][index]
    }

}
