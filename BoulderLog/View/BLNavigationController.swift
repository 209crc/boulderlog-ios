//
//  BLNavigationController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/12/06.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class BLNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationBar.barTintColor = ColorConst.Primary()
        navigationBar.tintColor = UIColor.yellow
        navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.white
        ]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
