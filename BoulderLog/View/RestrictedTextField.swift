//
//  RestrictedTextField.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/17.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class RestrictedTextField: UITextField {

    // 範囲選択カーソル非表示
    override func selectionRects(for range: UITextRange) -> [Any] {
        return []
    }
    
    // コピー・ペースト・選択等のメニュー非表示
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
