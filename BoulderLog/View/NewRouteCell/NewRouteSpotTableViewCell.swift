//
//  NewRouteSpotTableViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/10.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class NewRouteSpotTableViewCell: UITableViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(spot: Spot) {
        if !spot.name.isEmpty {
            valueLabel.text = spot.name
            valueLabel.textColor = UIColor.black
        } else {
            valueLabel.text = "未選択"
            valueLabel.textColor = UIColor.lightGray
        }
    }

}
