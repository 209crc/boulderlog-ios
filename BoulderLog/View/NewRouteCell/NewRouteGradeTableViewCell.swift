//
//  NewRouteGradeTableViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/10.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

protocol selectGradeDelegate {
    func didSelectGrade(grade: Int)
}

class NewRouteGradeTableViewCell: UITableViewCell, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    let valuePickerViewSection = 0

    @IBOutlet weak var valueTextField: RestrictedTextField!

    var valuePickerView: UIPickerView = UIPickerView()

    var delegate: selectGradeDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        valuePickerView.delegate = self
        valuePickerView.dataSource = self
        valuePickerView.showsSelectionIndicator = true

        valueTextField.delegate = self
        
        let valueToolBar = UIToolbar(frame: CGRect(x:0, y:0, width:self.frame.width, height:44))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton =  UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(valueDoneClicked(sender:)))
        valueToolBar.setItems([spacer, doneButton], animated: true)
        
        valueTextField.inputView = valuePickerView
        valueTextField.inputAccessoryView = valueToolBar

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: IBAction
    @objc internal func valueDoneClicked(sender: UIButton){
        self.valueTextField.endEditing(true)
    }

    // MARK: Functions
    func finishedEditingValueTextField(row: Int) {
        self.valueTextField.text = GradeManager.gradeString(index: row)
        delegate?.didSelectGrade(grade: GradeManager.gradeValue(index: row))
    }

    // MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField:UITextField){
        self.finishedEditingValueTextField(row: valuePickerView.selectedRow(inComponent: valuePickerViewSection))
    }
    
    // MARK: UIPickerViewDelegate, UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return valuePickerViewSection + 1
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    numberOfRowsInComponent component: Int) -> Int {
        return GradeManager.numberOfGrades()
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    titleForRow row: Int,
                    forComponent component: Int) -> String? {
        return GradeManager.gradeString(index: row)
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int) {
        self.finishedEditingValueTextField(row: row)
    }
}
