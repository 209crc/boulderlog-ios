//
//  NewRouteImageTableViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/10.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class NewRouteImageTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var images: Array<UIImage> = []
    var context = NewRouteViewController()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: UICollectionViewDelegate, UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if images.count + 1 > RouteImagesMax {
            return RouteImagesMax
        }
        return images.count + 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell = imageCollectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath as IndexPath) as! NewRouteImageCollectionViewCell
        imageCell.delegate = context
        imageCell.configure(images: images, indexPath: indexPath, context: context)
        return imageCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

    // MARK: method
    static func cellHeight() -> CGFloat {
        return 268.0
    }
    
    func configure(images: Array<UIImage>, context: NewRouteViewController) {
        self.context = context
        self.images = images
    }
}
