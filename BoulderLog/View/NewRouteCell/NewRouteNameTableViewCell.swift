//
//  NewRouteNameTableViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/10.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class NewRouteNameTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var valueTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        valueTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        valueTextField.resignFirstResponder()
        return true
    }

}
