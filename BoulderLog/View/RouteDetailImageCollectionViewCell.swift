//
//  RouteDetailImageCollectionViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/01.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import AlamofireImage

class RouteDetailImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(image: UIImage) {
        // todo : nilチェック
        imageView!.image = image
    }
}
