//
//  RouteImageDetailCollectionViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/10/29.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class RouteImageDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(image: UIImage) {
        imageView.image = image
    }
}
