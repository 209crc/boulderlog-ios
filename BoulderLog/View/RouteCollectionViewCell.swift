//
//  RouteCollectionViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/26.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import AlamofireImage

class RouteCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    
    // cellタップからのrouteDetail遷移時のために、propertyにしている
    // todo : 直す
    var route = Route()
    
    func configure(route: Route) {
        self.route = route
        
        if !route.image_url1.isEmpty {
            imageView!.af_setImage(withURL: URL(string: route.image_url1)!)
        }
        
        nameLabel.text = route.name
        gradeLabel.text = route.gradeString()
        
    }
}
