//
//  NewRouteImageCollectionViewCell.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/16.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

protocol selectImageDelegate {
    func didSelectImage(image: UIImage, row: Int)
    func didCancelSelectImage()
    func didStartEditImage(row: Int)
    func didRemoveImage(row: Int)
}

class NewRouteImageCollectionViewCell: UICollectionViewCell, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    
    var context = UIViewController()
    var delegate: selectImageDelegate?
    
    var indexPath = IndexPath()

    func configure(images: Array<UIImage>, indexPath: IndexPath, context: UIViewController) {
        self.context = context
        self.indexPath = indexPath
        
        if indexPath.row < images.count {
            imageView!.image = images[indexPath.row]
            addButton.isHidden = true
            editButton.isHidden = false
            removeButton.isHidden = false
        } else {
            imageView!.image = UIImage(named: "noImage200")!
            addButton.isHidden = false
            editButton.isHidden = true
            removeButton.isHidden = true
        }
        
        addButton.layer.cornerRadius = 8.0
        editButton.layer.cornerRadius = 8.0
        removeButton.layer.cornerRadius = 8.0
        
        // todo : 共通化する
        addButton.setImage(addButton.image(for: UIControlState.normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
//        addButton.tintColor = ColorConst.Primary()
        addButton.tintColor =  UIColor.white

        editButton.setImage(editButton.image(for: UIControlState.normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
//        editButton.tintColor = ColorConst.Primary()
        editButton.tintColor =  UIColor.white

        removeButton.setImage(removeButton.image(for: UIControlState.normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
//        removeButton.tintColor = ColorConst.Primary()
        removeButton.tintColor = UIColor.white

    }
    
    // MARK: Actions
    @IBAction func tappedAddButton(_ sender: UIButton) {
        // Action Sheet
        let actionSheet:UIAlertController = UIAlertController(title:"画像追加",
                                                              message: nil,
                                                              preferredStyle: UIAlertControllerStyle.actionSheet)
        
        // キャンセル
        let cancelAction:UIAlertAction = UIAlertAction(title: "キャンセル",
                                                       style: UIAlertActionStyle.cancel,
                                                       handler:nil)
        
        // カメラ
        let cameraAction:UIAlertAction = UIAlertAction(title: "カメラを起動",
                                                       style: UIAlertActionStyle.default,
                                                       handler: {(action:UIAlertAction!) -> Void in
                                                        self.showImagePicker(isCamera: true)
        })
        
        // カメラロール
        let camerarollAction:UIAlertAction = UIAlertAction(title: "カメラロールから選択",
                                                           style: UIAlertActionStyle.default,
                                                           handler: {(action:UIAlertAction!) -> Void in
                                                            self.showImagePicker(isCamera: false)
        })

        actionSheet.addAction(cancelAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(camerarollAction)
        
        context.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func tappedEditButton(_ sender: UIButton) {
        self.showEditImage()
    }
    
    @IBAction func tappedRemoveButton(_ sender: UIButton) {
        let alert:UIAlertController = UIAlertController(title:"確認",
                                                        message: "削除しますか?",
                                                        preferredStyle: UIAlertControllerStyle.alert)
        let settingAction:UIAlertAction = UIAlertAction(title: "削除",
                                                        style: UIAlertActionStyle.destructive,
                                                        handler: {(action:UIAlertAction!) -> Void in
                                                            self.delegate?.didRemoveImage(row: self.indexPath.row)
        })
        let okAction:UIAlertAction = UIAlertAction(title: "キャンセル",
                                                   style: UIAlertActionStyle.cancel,
                                                   handler:nil)
        
        alert.addAction(settingAction)
        alert.addAction(okAction)
        
        self.context.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Methods
    func showImagePicker(isCamera: Bool) {
        if (isCamera && !UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            // カメラ使用設定で、カメラ使用不可の場合
            let alert:UIAlertController = UIAlertController(title:"カメラを起動できません",
                                                            message: "設定アプリから\n写真へのアクセスを許可してください",
                                                            preferredStyle: UIAlertControllerStyle.alert)
            let okAction:UIAlertAction = UIAlertAction(title: "OK",
                                                       style: UIAlertActionStyle.default,
                                                       handler:nil)
            let settingAction:UIAlertAction = UIAlertAction(title: "設定",
                                                            style: UIAlertActionStyle.default,
                                                            handler: {(action:UIAlertAction!) -> Void in
                                                                // todo : 実機で動作確認
                                                                UIApplication.shared.open(URL(string: "App-Prefs:root=Photos")!, options: [:], completionHandler: nil)
            })
            
            alert.addAction(okAction)
            alert.addAction(settingAction)
            
            self.context.present(alert, animated: true, completion: nil)
            return
        }
        
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        
        if isCamera {
            cameraPicker.sourceType = UIImagePickerControllerSourceType.camera
        } else {
            cameraPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        
        self.context.present(cameraPicker, animated: true, completion: nil)
    }
    
    func showEditImage() {
        delegate?.didStartEditImage(row: self.indexPath.row)
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ imagePicker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var didSucceedSelectImage = false
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            delegate?.didSelectImage(image: pickedImage, row: self.indexPath.row)
            didSucceedSelectImage = true
        }
        imagePicker.dismiss(animated: true, completion: nil)
        
        if didSucceedSelectImage {
            self.showEditImage()
        }

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        delegate?.didCancelSelectImage()
    }
}
