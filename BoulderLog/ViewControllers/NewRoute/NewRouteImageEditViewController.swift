//
//  NewRouteImageEditViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/17.
//  Copyright © 2018年 209crc. All rights reserved.
//
// todo : この画面は端末回転に対応したい
// todo : ライセンス表記 https://github.com/acerbetti/ACEDrawingView

import UIKit
import ACEDrawingView

protocol editImageDelegate {
    func didEditImage(image: UIImage, row: Int)
}

class NewRouteImageEditViewController: UIViewController, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, ACEDrawingViewDelegate {
    
    enum LineColorWidth: Int {
        case color
        case width
        case max
    }
    
    let lineWidthCoefficient: CGFloat = 4.0
    let stampBorderWidth: CGFloat = 1.0

    var delegate: editImageDelegate?

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var drawingView: ACEDrawingView!
    @IBOutlet weak var baseImageView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    
    var selectedToolButton: UIBarButtonItem?
    var selectedColor: UIColor?
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var stampButton: UIBarButtonItem!
    @IBOutlet var circleButton: UIBarButtonItem!
    @IBOutlet var squareButton: UIBarButtonItem!
    @IBOutlet var lineButton: UIBarButtonItem!
    @IBOutlet var freeHandButton: UIBarButtonItem!
    @IBOutlet var textButton: UIBarButtonItem!
    @IBOutlet var designButton: UIBarButtonItem!
    @IBOutlet var redoButton: UIBarButtonItem!
    @IBOutlet var undoButton: UIBarButtonItem!

    @IBOutlet var stampStartButton: UIButton!
    @IBOutlet var stampGoalButton: UIButton!
    @IBOutlet var stampZoneButton: UIButton!
    @IBOutlet var stampLeftHandButton: UIButton!
    @IBOutlet var stampRightHandButton: UIButton!
    @IBOutlet var stampHandButton: UIButton!
    @IBOutlet var stampLeftFootButton: UIButton!
    @IBOutlet var stampRightFootButton: UIButton!
    @IBOutlet var stampFootButton: UIButton!
    @IBOutlet var stampJugButton: UIButton!
    @IBOutlet var stampPinchButton: UIButton!
    @IBOutlet var stampSloperButton: UIButton!
    @IBOutlet var stampCrimpButton: UIButton!
    @IBOutlet var stampPocketButton: UIButton!
    @IBOutlet var stampGradeButton: UIButton!
    @IBOutlet var stampFootRestrictedButton: UIButton!
    @IBOutlet var stampFootNotRestrictedButton: UIButton!
    @IBOutlet var stampCornerOkButton: UIButton!
    @IBOutlet var stampCornerNgButton: UIButton!
    @IBOutlet var stampAngleButton: UIButton!
    @IBOutlet var stampAngleSlider: UISlider!
    @IBOutlet var stampToolBarTopConstraint: NSLayoutConstraint!

    @IBOutlet var lineColorWidthPicker: UIPickerView!
    @IBOutlet var lineColorWidthToolBar: UIToolbar!
    @IBOutlet var lineColorWidthToolBarTopConstraint: NSLayoutConstraint!

    var image = UIImage()
    var imageIndexPathRow = Int()
    var grade = GradeManagerGradeUndefined
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        baseImageView.image = image
        drawingView.frame = AVMakeRect(aspectRatio: image.size, insideRect: self.baseImageView.frame)
        
        self.scrollView.delegate = self
        self.drawingView.delegate = self;
        
        self.lineColorWidthPicker.delegate = self
        self.lineColorWidthPicker.dataSource = self
        
        self.drawingView.draggableTextFontName = "Helvetica"
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action:#selector(self.doubleTap))
        doubleTapGesture.numberOfTapsRequired = 2
        self.wrapperView.addGestureRecognizer(doubleTapGesture)
        
        self.drawingView.lineColor = UIColor.red
        self.drawingView.lineWidth = 1.0 * lineWidthCoefficient
        self.selectedColor = UIColor.red
        
        self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke
        self.selectedToolButton = circleButton
        setItemsColor()
        designButton.tintColor = UIColor.black
        redoButton.tintColor = UIColor.black
        undoButton.tintColor = UIColor.black
        
        self.stampLeftHandButton.layer.borderWidth = stampBorderWidth
        self.stampLeftHandButton.layer.borderColor = UIColor.black.cgColor;
        self.stampRightHandButton.layer.borderWidth = stampBorderWidth
        self.stampRightHandButton.layer.borderColor = UIColor.black.cgColor;
        self.stampHandButton.layer.borderWidth = stampBorderWidth
        self.stampHandButton.layer.borderColor = UIColor.black.cgColor;
        self.stampLeftFootButton.layer.borderWidth = stampBorderWidth
        self.stampLeftFootButton.layer.borderColor = UIColor.black.cgColor;
        self.stampRightFootButton.layer.borderWidth = stampBorderWidth
        self.stampRightFootButton.layer.borderColor = UIColor.black.cgColor;
        self.stampFootButton.layer.borderWidth = stampBorderWidth
        self.stampFootButton.layer.borderColor = UIColor.black.cgColor;
        self.stampJugButton.layer.borderWidth = stampBorderWidth
        self.stampJugButton.layer.borderColor = UIColor.black.cgColor;
        self.stampPinchButton.layer.borderWidth = stampBorderWidth
        self.stampPinchButton.layer.borderColor = UIColor.black.cgColor;
        self.stampSloperButton.layer.borderWidth = stampBorderWidth
        self.stampSloperButton.layer.borderColor = UIColor.black.cgColor;
        self.stampCrimpButton.layer.borderWidth = stampBorderWidth
        self.stampCrimpButton.layer.borderColor = UIColor.black.cgColor;
        self.stampPocketButton.layer.borderWidth = stampBorderWidth
        self.stampPocketButton.layer.borderColor = UIColor.black.cgColor;
        
        self.stampGradeButton.setTitle(GradeManager.gradeString(grade: grade), for: UIControlState.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction
    @IBAction func backButtonTapped(sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTapped(sender:UIButton) {
        self.drawingView.prepareForSnapshot()
        
        let baseImage = self.baseImageView.image
        let saveImage = (baseImage != nil) ? self.drawingView.applyDraw(to: baseImage) : self.drawingView.image

        self.delegate?.didEditImage(image: saveImage!, row: imageIndexPathRow)
        
        self.dismiss(animated: true, completion: nil);
    }
    
    // MARK: IBAction (Setting)
    @IBAction func stampButtonTapped(sender:UIBarButtonItem) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            self.stampToolBarTopConstraint.isActive = false
            self.view.layoutIfNeeded()
        },completion:nil)
    }
    
    @IBAction func closeStampButtonTapped(sender:UIBarButtonItem) {
        self.closeStampView()
    }
    
    func closeStampView() {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            self.stampToolBarTopConstraint.isActive = true
            self.view.layoutIfNeeded()
        },completion:nil)
    }
    
    @IBAction func circleButtonTapped(sender:UIBarButtonItem) {
        self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke
        
        selectedToolButton = sender
        setItemsColor()
    }
    
    @IBAction func squareuttonTapped(sender:UIBarButtonItem) {
        self.drawingView.drawTool = ACEDrawingToolTypeRectagleStroke

        selectedToolButton = sender
        setItemsColor()
    }
    
    @IBAction func lineButtonTapped(sender:UIBarButtonItem) {
        self.drawingView.drawTool = ACEDrawingToolTypeLine
        
        selectedToolButton = sender
        setItemsColor()
    }
    
    @IBAction func freeHandButtonTapped(sender:UIBarButtonItem) {
        self.drawingView.drawTool = ACEDrawingToolTypePen

        selectedToolButton = sender
        setItemsColor()
    }
    
    @IBAction func textButtonTapped(sender:UIBarButtonItem) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableText
        
        selectedToolButton = sender
        setItemsColor()
    }
    
    @IBAction func designButtonTapped(sender:UIBarButtonItem) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            self.lineColorWidthToolBarTopConstraint.isActive = false
            self.view.layoutIfNeeded()
        },completion:nil)
    }
    @IBAction func closeDesignButtonTapped(sender:UIBarButtonItem) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
            self.lineColorWidthToolBarTopConstraint.isActive = true
            self.view.layoutIfNeeded()
        },completion:nil)
    }
    
    @IBAction func redoButtonTapped(sender:UIBarButtonItem) {
        self.drawingView.redoLatestStep()
        self.updateButtonStatus()
    }
    
    @IBAction func undoButtonTapped(sender:UIBarButtonItem) {
        self.drawingView.undoLatestStep()
        self.updateButtonStatus()
    }
    
    // MARK: IBAction (Setting - Stamp)
    @IBAction func tappedStampStartButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextStart
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }
    
    @IBAction func tappedStampGoalButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextGoal
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampZoneButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextZone
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()}

    @IBAction func tappedStampLeftHandButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextLeftHand
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampRightHandButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextRightHand
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampHandButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextHand
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampLeftFootButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextLeftFoot
        self.closeStampView()
    }

    @IBAction func tappedStampRightFootButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextRightFoot
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampFootButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextFoot
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampJugButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextJug
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }
    
    @IBAction func tappedStampPinchButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextPinch
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampSloperButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextSloper
        self.closeStampView()
    }

    @IBAction func tappedStampCrimpButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextCrimp
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampPocketButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextPocket
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampGradeButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextGrade
        self.drawingView.stampString = GradeManager.gradeString(grade: grade)
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampFootRestrictedButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextFootRestricted
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampFootNotRestrictedButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextFootNotRestricted
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampCornerOkButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextCornerOk
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampCornerNgButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextCornerNg
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func tappedStampAngleButton(sender:UIButton) {
        self.drawingView.drawTool = ACEDrawingToolTypeDraggableTextAngle
        self.drawingView.stampString = stampAngleButton.title(for: UIControlState.normal)!
        self.closeStampView()
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    @IBAction func changedStampAngleSlider(sender:UISlider) {
        let sliderValueInt = Int(sender.value)
        let angleString = String(sliderValueInt - (sliderValueInt % 5))
        stampAngleButton.setTitle(angleString + "°", for: UIControlState.normal)
        stampAngleButton.setTitle(angleString + "°", for: UIControlState.highlighted)
        
        selectedToolButton = stampButton
        setItemsColor()
    }

    // MARK: Methods
    func updateButtonStatus() {
        self.undoButton.isEnabled = self.drawingView.canUndo()
        self.redoButton.isEnabled = self.drawingView.canRedo()
    }
    
    func setItemsColor() {
        let alpha: CGFloat = 0.4
        stampButton.tintColor = UIColor.black.withAlphaComponent(alpha)
        circleButton.tintColor = selectedColor?.withAlphaComponent(alpha)
        squareButton.tintColor = selectedColor?.withAlphaComponent(alpha)
        lineButton.tintColor = selectedColor?.withAlphaComponent(alpha)
        freeHandButton.tintColor = selectedColor?.withAlphaComponent(alpha)
        
        if selectedToolButton == stampButton {
            selectedToolButton?.tintColor = UIColor.black
        } else {
            selectedToolButton?.tintColor = selectedColor
        }
    }
    
    // todo : フリーハンドで描画中はスクロールしないとか考えないと
    // MARK: Scale Methods
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.wrapperView
    }
    
    @objc func doubleTap(gesture: UITapGestureRecognizer) -> Void {
        let maxZoomScale: CGFloat = 3.0
        let doubleTapZoomScale: CGFloat = 2.0
        self.scrollView.maximumZoomScale = maxZoomScale
        
        if (self.scrollView.zoomScale < maxZoomScale / 2) {
            let newScale = self.scrollView.zoomScale * doubleTapZoomScale
            let zoomRect = self.zoomRectForScale(scale: newScale, center: gesture.location(in: gesture.view))
            self.scrollView.zoom(to: zoomRect, animated: true)
        } else {
            self.scrollView.setZoomScale(1.0, animated: true)
        }
    }
    
    func zoomRectForScale(scale:CGFloat, center: CGPoint) -> CGRect{
        let size = CGSize(
            width: self.scrollView.frame.size.width / scale,
            height: self.scrollView.frame.size.height / scale
        )
        return CGRect(
            origin: CGPoint(
                x: center.x - size.width / 2.0,
                y: center.y - size.height / 2.0
            ),
            size: size
        )
    }
    
    // MARK: UIPickerViewDelegate, UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return LineColorWidth.max.rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == LineColorWidth.color.rawValue ? 12 : 6;
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let height: CGFloat = 46.0
        
        if (component == LineColorWidth.color.rawValue) {
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: pickerView.frame.size.width / 2.0, height: height)
            
            let colorView = UIView()
            colorView.frame = CGRect(x: 32.0, y: 8.0, width: (pickerView.frame.size.width / 2.0) - 64.0, height: 30.0)
            colorView.backgroundColor = self.colorForPickerView(row: row)
            colorView.layer.borderColor = UIColor.black.cgColor
            colorView.layer.borderWidth = 1.0
            view.addSubview(colorView)
            
            return view
        } else {
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: pickerView.frame.size.width / 2.0, height: height)

            let widthView = UIView()
            widthView.frame = CGRect(x: 32.0,
                                     y: (height - ((CGFloat)(row + 1) * lineWidthCoefficient)) / 2.0,
                                     width: (pickerView.frame.size.width / 2.0) - 64.0,
                                     height: CGFloat((CGFloat)(row + 1) * lineWidthCoefficient))
            widthView.backgroundColor = drawingView.lineColor
            widthView.layer.cornerRadius = ((CGFloat)(row + 1) * lineWidthCoefficient) / 2.0
            widthView.layer.borderColor = UIColor.black.cgColor
            widthView.layer.borderWidth = 1.0
            view.addSubview(widthView)
            
            return view
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (component == LineColorWidth.color.rawValue) {
            drawingView.lineColor = self.colorForPickerView(row: row)
            selectedColor = self.colorForPickerView(row: row)
            setItemsColor()
            pickerView.reloadComponent(LineColorWidth.width.rawValue)
        } else {
            drawingView.lineWidth = CGFloat((CGFloat)(row + 1) * lineWidthCoefficient)
        }
    }
    
    func colorForPickerView(row: Int) -> UIColor {
        switch row {
        case 0:
            return UIColor.red
        case 1:
            return UIColor.magenta
        case 2:
            return UIColor.orange
        case 3:
            return UIColor.yellow
        case 4:
            return UIColor.brown
        case 5:
            return UIColor.green
        case 6:
            return UIColor.cyan
        case 7:
            return UIColor.blue
        case 8:
            return UIColor.purple
        case 9:
            return UIColor.black
        case 10:
            return UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        case 11:
            return UIColor.white
        default:
            return UIColor.red
        }
    }
    
    // MARK: ACEDrawing View Delegate
    func drawingView(_ view: ACEDrawingView!, didEndDrawUsing tool: ACEDrawingTool!) {
        self.updateButtonStatus()
    }
    
    // todo : 必要かどうか確認する
//    - (void)drawingView:(ACEDrawingView *)view configureTextToolLabelView:(ACEDrawingLabelView *)label;
//    {
//    // If you don't like the default text control styles, you can tweak them
//    // in this delegate method.
//    label.shadowOffset = CGSizeMake(0, 1);
//    label.shadowOpacity = 0.5;
//    label.shadowRadius = 1;
//    label.closeButtonOffset = CGPointMake(-6, -6);
//    label.rotateButtonOffset = CGPointMake(6, 6);
//    }
}
