//
//  NewRouteSpotSelectViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/12.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

protocol selectSpotDelegate {
    func didSelectSpot(spot: Spot)
}

class NewRouteSpotSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    let TableViewSectionHeaderHeight: CGFloat = 30.0
    let TableViewRowHeight: CGFloat = 30.0

//    @IBOutlet weak var favoriteSegmentedControl: UISegmentedControl!        // todo : お気に入り
    @IBOutlet weak var spotsTableView: UITableView!
    
    @IBOutlet weak var backBarButton: UIBarButtonItem!
    @IBOutlet weak var selectBarButton: UIBarButtonItem!

    var delegate: selectSpotDelegate?
    
    var selectedSpot = Spot()                   // 選択したcellのspot。 todo : もっといいやり方はないだろうか

    private var openedSpotsTableViewSections = Set<Int>()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        spotsTableView.delegate = self
        spotsTableView.dataSource = self
        
        selectBarButton.isEnabled = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction
    @IBAction func backButtonTapped(sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectButtonTapped(sender:UIButton) {
        delegate?.didSelectSpot(spot: selectedSpot)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Methods
    @objc func tapSpotsTableViewSectionHeader(_ sender: UITapGestureRecognizer){
        if sender.state == .ended {
            if let section = sender.view?.tag {
                if self.openedSpotsTableViewSections.contains(section) {
                    self.openedSpotsTableViewSections.remove(section)
                } else {
                    self.openedSpotsTableViewSections.insert(section)
                }
                self.spotsTableView.reloadSections(IndexSet(integer: section), with: .fade)
            }
        }
    }

    // MARK: UITableViewDataSource, UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        if !SpotsManager.shared.isSpotsSetted() {
            return 0
        }
        return SpotsManager.shared.getPrefectureCount()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return TableViewSectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SpotsManager.shared.isSpotsSetted() && self.openedSpotsTableViewSections.contains(section) {
            return SpotsManager.shared.getSpots(prefectureCode: section).count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = UITableViewHeaderFooterView()
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapSpotsTableViewSectionHeader(_:)))
        tapGesture.delegate = self
        sectionHeaderView.addGestureRecognizer(tapGesture)
        sectionHeaderView.tag = section
        
        return sectionHeaderView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return SpotsManager.shared.getPrefectureList()[section]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "spotCell", for: indexPath)
        cell.textLabel?.text = SpotsManager.shared.getSpot(prefectureCode: indexPath.section, index: indexPath.row).name
        return cell
    }
    
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath) {
        // 再タップ時に選択解除する場合、selectBarButton.isEnabled = falseにすること
        selectBarButton.isEnabled = true

        selectedSpot = SpotsManager.shared.getSpot(prefectureCode: indexPath.section, index: indexPath.row)
        
        let cell = tableView.cellForRow(at:indexPath)
        cell?.accessoryType = .checkmark
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at:indexPath)
        cell?.accessoryType = .none
    }
}
