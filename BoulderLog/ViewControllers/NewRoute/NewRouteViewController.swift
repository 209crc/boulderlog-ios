//
//  NewRouteViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/05/31.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class NewRouteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, selectGradeDelegate, selectImageDelegate, selectSpotDelegate, editImageDelegate {
    
    let SegueNewRouteToImageEdit = "NewRouteToImageEdit"
    let SegueNewRouteToSpotSelect = "NewRouteToSpotSelect"
    let NewRouteTableViewRow = 0    // 1つしか使わない前提
    
    enum NewRouteSection: Int {
        case spot
        case name
        case grade
        case image
        case max
    }
    @IBOutlet weak var newRouteTableView: UITableView!
    
    var images: Array<UIImage> = []
    public var selectedSpot = Spot()
    var grade = GradeManagerGradeMin
    
    var editingImageIndexPathRow = Int()  // 画像編集開始時(didStartEditImageが呼ばれた時)から、画像編集画面を開く時(segueのprepare)の間だけ使う変数
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        newRouteTableView.delegate = self
        newRouteTableView.dataSource = self
        
        newRouteTableView.tableFooterView = UIView()
        
        let footerCell: UITableViewCell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteFooter")!
        let footerView: UIView = footerCell.contentView
        newRouteTableView.tableFooterView = footerView
        
        // todo : 消す
        images.append(UIImage(named: "tmpWallStub")!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPathForSelectedRow = newRouteTableView.indexPathForSelectedRow {
            newRouteTableView.deselectRow(at: indexPathForSelectedRow, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewRouteSection.max.rawValue
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case NewRouteSection.image.rawValue:
            return NewRouteImageTableViewCell.cellHeight()
        default:
            return 44.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case NewRouteSection.spot.rawValue:
            let cell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteSpotCell") as! NewRouteSpotTableViewCell
            cell.configure(spot: selectedSpot)
            return cell
        case NewRouteSection.name.rawValue:
            let cell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteNameCell") as! NewRouteNameTableViewCell
            return cell
        case NewRouteSection.grade.rawValue:
            let cell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteGradeCell") as! NewRouteGradeTableViewCell
            cell.delegate = self
            return cell
        case NewRouteSection.image.rawValue:
            let cell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteImageCell") as! NewRouteImageTableViewCell
            cell.configure(images: images, context: self)
            return cell
        default:
            // 想定外
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case NewRouteSection.spot.rawValue:
            self.performSegue(withIdentifier: SegueNewRouteToSpotSelect, sender: nil)
        default: break
        }
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == SegueNewRouteToSpotSelect) {
            let newRouteSpotSelectViewController: NewRouteSpotSelectViewController = (segue.destination as? NewRouteSpotSelectViewController)!
            newRouteSpotSelectViewController.delegate = self
        } else if (segue.identifier == SegueNewRouteToImageEdit) {
            let newRouteImageEditViewController: NewRouteImageEditViewController = (segue.destination as? NewRouteImageEditViewController)!
            newRouteImageEditViewController.delegate = self
            newRouteImageEditViewController.image = images[editingImageIndexPathRow]
            newRouteImageEditViewController.grade = grade
            newRouteImageEditViewController.imageIndexPathRow = editingImageIndexPathRow
        }
    }
    
    // MARK: selectGradeDelegate
    func didSelectGrade(grade: Int) {
        self.grade = grade
    }
    
    // MARK: selectImageDelegate
    func didSelectImage(image: UIImage, row: Int) {
        if images.count > RouteImagesMax {
            return  // 想定外
        }
        
        if images.count - 1 < row {
            images.insert(image, at: row)
        } else {
            images[row] = image
        }
        
        // todo : 全体を読み込み直す必要はないはず
        newRouteTableView.reloadData()
        
        let cell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteImageCell") as! NewRouteImageTableViewCell
        cell.configure(images: images, context: self)
    }
    
    func didCancelSelectImage() {
        
    }
    
    func didStartEditImage(row: Int) {
        editingImageIndexPathRow = row
        self.performSegue(withIdentifier: SegueNewRouteToImageEdit, sender: nil)
    }
    
    func didRemoveImage(row: Int) {
        images.remove(at: row)
        
        // todo : 全体を読み込み直す必要はないはず
        newRouteTableView.reloadData()
        
        let cell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteImageCell") as! NewRouteImageTableViewCell
        cell.configure(images: images, context: self)
    }
    
    // MARK: selectSpotDelegate
    func didSelectSpot(spot: Spot) {
        self.selectedSpot = spot
        // todo : 全体を読み込み直す必要はないはず
        newRouteTableView.reloadData()
    }
    
    // MARK : editImageDelegate
    func didEditImage(image: UIImage, row: Int) {
        self.images[row] = image
        newRouteTableView.reloadData()
        
        let cell = newRouteTableView.dequeueReusableCell(withIdentifier: "NewRouteImageCell") as! NewRouteImageTableViewCell
        cell.configure(images: images, context: self)
    }
    
    // MARK: Action
    @IBAction func postRouteButtonTapped(sender:UIButton) {
        let saveAlert = UIAlertController(title: "確認", message: "投稿しますか?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel) { (action) -> Void in
        }
        
        let saveAction = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
            // 気が向いたら、きれいにかく
            let nameCell = self.newRouteTableView.cellForRow(at: IndexPath.init(row: NewRouteSection.name.rawValue, section: self.NewRouteTableViewRow)) as! NewRouteNameTableViewCell
            
            if let nameText = nameCell.valueTextField.text {
                LoadingViewManager.shared.show()
                
                // todo : ユーザID設定する
                APICaller.shared.postRouteCreate(spotId: self.selectedSpot.id,
                                                 userId: 1,
                                                 name: nameText,
                                                 grade: self.grade,
                                                 successHandler: {
                                                    LoadingViewManager.shared.dismiss()
                                                    Utils.showAlert(context: self, title: "完了", message: "")},
                                                 errorHandler: {(apiError) in
                                                    LoadingViewManager.shared.dismiss()
                                                    Utils.showAlert(context: self, title: "エラー", message: "投稿に失敗しました\nご不明な点はお問い合わせください")
                })
                
            }
        }
        
        saveAlert.addAction(cancelAction)
        saveAlert.addAction(saveAction)
        
        present(saveAlert, animated: true, completion: nil)
    }
    
}

