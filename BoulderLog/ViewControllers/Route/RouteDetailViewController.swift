//
//  RouteDetailViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/01.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import Alamofire

class RouteDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let SegueRouteToImageDetail = "routeToImageDetail"
    let ImageCollectionViewIndexPathSection = 0     // 0しか使わない前提

    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    @IBOutlet weak var updateDateLabel: UILabel!
    @IBOutlet weak var removedDateLabel: UILabel!

    var route = Route()
    var routeImages:Array<UIImage> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self

        nameLabel.text = route.name
        gradeLabel.text = route.gradeString()
        updateDateLabel.text = Utils.dateYmd(unixTime: route.updated_at)
        removedDateLabel.text = Utils.dateYmd(unixTime: route.removed_at)
        
        let stubImage = UIImage.init(named: "noImage200")!
        for _ in 0..<route.numberOfImageUrls() {
            // 画像配列初期化用のforループ
            // 下のループと一緒にしてもいいと思うが、全部初期化する前に、画像1枚目の読込みが終わるとかあるとか考えるが面倒なので別にしている
            routeImages.append(stubImage)
        }
        
        for cnt in 0..<route.numberOfImageUrls() {
            Alamofire.request(self.route.imageUrlStringOfIndex(index: cnt)).responseImage { response in
                if let picture = response.result.value {
                    self.routeImages[cnt] = picture
                    self.imageCollectionView.reloadData()   // todo : 全部読み込み直さなくてもいいはず
                    
                    // todo : この時点imageDetailViewControllerを作っておいて、imagesを渡すようにする
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDelegate, UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return route.numberOfImageUrls()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell = imageCollectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath as IndexPath) as! RouteDetailImageCollectionViewCell
        imageCell.configure(image: routeImages[indexPath.row])
        return imageCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: self.SegueRouteToImageDetail, sender: nil)
    }

    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == SegueRouteToImageDetail) {
            let routeImageDetailViewController: RouteImageDetailViewController = (segue.destination as? RouteImageDetailViewController)!
            routeImageDetailViewController.modalPresentationStyle = .overCurrentContext
            routeImageDetailViewController.images = routeImages
        }
    }

}
