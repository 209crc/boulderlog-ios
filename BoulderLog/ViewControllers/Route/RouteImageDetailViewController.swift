//
//  RouteImageDetailViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/07/05.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

// todo : タップした画像が表示されるようにする(現状、1番目が表示される)
// todo : 画像拡大した後に、タブ切替えると閉じた後、黒画面になるので直す
// todo : 画像拡大できるようにする

class RouteImageDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    var images: Array<UIImage>!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        
        setupClosePanGesture()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK : UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:RouteImageDetailCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageDetailCell", for: indexPath) as! RouteImageDetailCollectionViewCell
        cell.configure(image: images[indexPath.row])

        return  cell
    }

    // MARK : UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
}

// https://qiita.com/k0uhashi/items/709da728af4c0bc9c0b3
extension RouteImageDetailViewController {
    
    enum CloseDirection {
        case up
        case down
    }
    
    private func setupClosePanGesture() {
        // スワイプ開始時の位置を格納
        var startPanPointY: CGFloat = 0.0
        // スワイプ開始時の位置とCollectionViewのCenterの距離を格納
        var distanceY: CGFloat = 0.0
        // 画面を閉じるラインの設定　（画面高さの1/6の距離を移動したら）
        let moveAmountYCloseLine: CGFloat = view.bounds.height / 6
        let minBackgroundAlpha: CGFloat = 0.5
        let maxBackgroundAlpha: CGFloat = 1.0
        
        let panGesture = UIPanGestureRecognizer(target: self, action: nil)
        panGesture.rx.event
            .subscribe(onNext: { [weak self] sender in
                guard let strongSelf = self else { return }
                
                let currentPointY = sender.location(in: strongSelf.view).y
                
                switch sender.state {
                case .began:
                    // スワイプを開始したら呼ばれる １回だけ
                    startPanPointY = currentPointY
                    distanceY = strongSelf.imagesCollectionView.center.y - startPanPointY
                    strongSelf.updateHeaderFooterView(isHidden: true)
                case .changed:
                    // スワイプ中呼ばれる　移動するたび
                    // CollectionViewの移動
                    let calcedCollectionViewPosition = CGPoint(x: strongSelf.view.bounds.width / 2, y: distanceY + currentPointY)
                    strongSelf.imagesCollectionView.center = calcedCollectionViewPosition
                    // 背景の透明度更新
                    let moveAmountY = fabs(currentPointY - startPanPointY)
                    var backgroundAlpha = moveAmountY / (-moveAmountYCloseLine) + 1
                    if backgroundAlpha > maxBackgroundAlpha {
                        backgroundAlpha = maxBackgroundAlpha
                    } else if backgroundAlpha < minBackgroundAlpha {
                        backgroundAlpha = minBackgroundAlpha
                    }
                    strongSelf.view.backgroundColor = strongSelf.view.backgroundColor?.withAlphaComponent(backgroundAlpha)
                case .ended:
                    // 指を離すと呼ばれる
                    let moveAmountY = currentPointY - startPanPointY
                    let isCloseTop = moveAmountY > moveAmountYCloseLine
                    let isCloseBottom = moveAmountY < moveAmountYCloseLine * -1
                    if isCloseTop {
                        strongSelf.dismiss(animateDuration: 0.15, direction: .up)
                        return
                    }
                    if isCloseBottom {
                        strongSelf.dismiss(animateDuration: 0.15, direction: .down)
                        return
                    }
                    UIView.animate(withDuration: 0.25, animations: {
                        strongSelf.imagesCollectionView.center = strongSelf.view.center
                        strongSelf.view.backgroundColor = strongSelf.view.backgroundColor?.withAlphaComponent(1.0)
                    })
                    strongSelf.updateHeaderFooterView(isHidden: true)
                default: break
                }
            })
            .disposed(by: disposeBag)
        self.view.addGestureRecognizer(panGesture)
    }
    
    
    private func dismiss(animateDuration: TimeInterval, direction: CloseDirection) {
        let collectionViewCenterPoint: CGPoint = {
            switch direction {
            case .up:
                return CGPoint(x: view.bounds.width / 2, y: view.bounds.height + imagesCollectionView.bounds.height)
            case .down:
                return CGPoint(x: view.bounds.width / 2, y: -imagesCollectionView.bounds.height)
            }
        }()
        UIView.animate(withDuration: animateDuration, animations: { [weak self] in
            self?.view.backgroundColor = self?.view.backgroundColor?.withAlphaComponent(0.0)
            self?.imagesCollectionView.center = collectionViewCenterPoint
            }, completion: { [weak self] _ in
                self?.dismiss(animated: false, completion: nil)
        })
    }
    
    // Twitterでいう、「リプライ」「お気に入り」などがあるViewの表示制御処理
    private func updateHeaderFooterView(isHidden: Bool) {
        print("isHidden = \(isHidden)")
    }
}
