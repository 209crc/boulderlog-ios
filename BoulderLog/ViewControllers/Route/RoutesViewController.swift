//
//  RoutesViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/24.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

// todo : グレードごとにソートする
// todo : フィルター使えるようにする

class RoutesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let SegueRoutesToSpotDetail = "routesToSpotDetail"
    let SegueRoutesToRouteDetail = "routesToRouteDetail"
    
    @IBOutlet weak var routeCollectionView: UICollectionView!

    var spot = Spot()
    var routes = [Route]()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        routeCollectionView.delegate = self
        routeCollectionView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    @IBAction func tappedSpotDetail(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: SegueRoutesToSpotDetail,sender: nil)
    }
    
    // MARK: UICollectionViewDelegate, UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return routes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let routeCell = routeCollectionView.dequeueReusableCell(withReuseIdentifier: "routeCell", for: indexPath as IndexPath) as! RouteCollectionViewCell
        routeCell.configure(route: routes[indexPath.row])
        
        return routeCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueRoutesToRouteDetail, sender: collectionView.cellForItem(at: indexPath))
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == SegueRoutesToSpotDetail) {
            let spotDetailViewController: SpotDetailViewController = (segue.destination as? SpotDetailViewController)!
            spotDetailViewController.spot = self.spot
        } else if (segue.identifier == SegueRoutesToRouteDetail) {
            let routeDetailViewController: RouteDetailViewController = (segue.destination as? RouteDetailViewController)!
            
            // todo : classチェック
            let senderCell = sender as! RouteCollectionViewCell
            routeDetailViewController.route = senderCell.route
        }
    }

}
