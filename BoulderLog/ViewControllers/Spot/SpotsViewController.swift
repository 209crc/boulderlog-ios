//
//  SpotsViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/05/31.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class SpotsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    let TableViewSectionHeaderHeight: CGFloat = 30.0
    let TableViewRowHeight: CGFloat = 30.0
    
    let SpotsOpenedSectionIndexUndefined = 0

    let SegueSpotsToRoutes = "spotsToRoutes"
    
//    @IBOutlet weak var favoriteSegmentedControl: UISegmentedControl!        // todo : お気に入り
    @IBOutlet weak var spotsTableView: UITableView!
    
    var selectedSpot = Spot()                   // 選択したcellのspot。 todo : もっといいやり方はないだろうか
    var selectedSpotRouteArr = [Route]()        // 選択したcellのspotのroutesArr。 todo : もっといいやり方はないだろうか

    private var openedSpotsTableViewSections = Set<Int>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        spotsTableView.delegate = self
        spotsTableView.dataSource = self
        
        // todo : 初回だけにする?
        APICaller.shared.getSpots(successHandler: {spotsArr in
            SpotsManager.shared.setSpots(spotsArr: spotsArr)
            self.spotsTableView.reloadData()
        }, errorHandler: {apiError in
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPathForSelectedRow = spotsTableView.indexPathForSelectedRow {
            spotsTableView.deselectRow(at: indexPathForSelectedRow, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Methods
    @objc func tapSpotsTableViewSectionHeader(_ sender: UITapGestureRecognizer){
        if sender.state == .ended {
            if let section = sender.view?.tag {
                if self.openedSpotsTableViewSections.contains(section) {
                    self.openedSpotsTableViewSections.remove(section)
                } else {
                    self.openedSpotsTableViewSections.insert(section)
                }
                self.spotsTableView.reloadSections(IndexSet(integer: section), with: .fade)
            }
        }
    }
    
    // MARK: UITableViewDataSource, UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        if !SpotsManager.shared.isSpotsSetted() {
            return 0
        }
        return SpotsManager.shared.getPrefectureCount()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return TableViewSectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SpotsManager.shared.isSpotsSetted() && self.openedSpotsTableViewSections.contains(section) {
            return SpotsManager.shared.getSpots(prefectureCode: section).count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = UITableViewHeaderFooterView()
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapSpotsTableViewSectionHeader(_:)))
        tapGesture.delegate = self
        sectionHeaderView.addGestureRecognizer(tapGesture)
        sectionHeaderView.tag = section
        
        return sectionHeaderView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return SpotsManager.shared.getPrefectureList()[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "spotCell", for: indexPath)        
        cell.textLabel?.text = SpotsManager.shared.getSpot(prefectureCode: indexPath.section, index: indexPath.row).name
        return cell
    }
    
    func tableView(_ table: UITableView,didSelectRowAt indexPath: IndexPath) {
        
        selectedSpot = SpotsManager.shared.getSpot(prefectureCode: indexPath.section, index: indexPath.row)
        
        LoadingViewManager.shared.show()
        APICaller.shared.getRoutes(spotId: selectedSpot.id, successHandler: {routesArr in
            LoadingViewManager.shared.dismiss()
            
            // todo : キャッシュする?
            self.selectedSpotRouteArr = routesArr
            
            self.performSegue(withIdentifier: self.SegueSpotsToRoutes,sender: nil)
        }, errorHandler: {apiError in
            LoadingViewManager.shared.dismiss()
        })
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == SegueSpotsToRoutes) {
            let routesViewController: RoutesViewController = (segue.destination as? RoutesViewController)!
            routesViewController.spot = selectedSpot
            routesViewController.routes = selectedSpotRouteArr
        }
    }
    
}

