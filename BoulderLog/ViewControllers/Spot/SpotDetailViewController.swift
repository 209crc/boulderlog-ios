//
//  SpotDetailViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/06/25.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class SpotDetailViewController: UIViewController {
    
    @IBOutlet weak var spotNameLabel: UILabel!
    @IBOutlet weak var spotAddressLabel: UILabel!
    
    var spot = Spot()

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // spotには値が入っている前提
        spotNameLabel.text = spot.name
        spotAddressLabel.text = spot.address
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
