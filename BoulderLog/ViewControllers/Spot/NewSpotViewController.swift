//
//  NewSpotViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/05/31.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit

class NewSpotViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    let prefecturePickerViewSection = 0
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var prefectureTextField: RestrictedTextField!
    @IBOutlet weak var saveButton: UIButton!

    var prefecturePickerView: UIPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prefecturePickerView.delegate = self
        prefecturePickerView.dataSource = self
        prefecturePickerView.showsSelectionIndicator = true
        
        prefectureTextField.delegate = self
        
        let prectureToolBar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:44))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton =  UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(prefectureDoneClicked(sender:)))
        prectureToolBar.setItems([spacer, doneButton], animated: true)
        
        prefectureTextField.inputView = prefecturePickerView
        prefectureTextField.inputAccessoryView = prectureToolBar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction
    @objc internal func prefectureDoneClicked(sender: UIButton){
        self.prefectureTextField.endEditing(true)
    }
    
    @IBAction func saveButtonTapped(sender:UIButton) {

        if nameTextField.text?.count == 0 {
            let inputErrorAlert = UIAlertController(title: "エラー", message: "スポット名が入力されていません", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            inputErrorAlert.addAction(okAction)
            present(inputErrorAlert, animated: true, completion: nil)

            // todo : 動いてない
            nameTextField.becomeFirstResponder()
            return;
        } else if prefectureTextField.text?.count == 0 {
            let inputErrorAlert = UIAlertController(title: "エラー", message: "都道府県が入力されていません", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            inputErrorAlert.addAction(okAction)
            present(inputErrorAlert, animated: true, completion: nil)
            
            // todo : 動いてない
            prefectureTextField.becomeFirstResponder()
            return;
        }
        
        let saveAlert = UIAlertController(title: "確認", message: "仮登録しますか?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel) { (action) -> Void in
        }
        
        let saveAction = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
            if let nameText = self.nameTextField.text {
                LoadingViewManager.shared.show()

                APICaller.shared.postSpotProvisionallyRegister(name: nameText,
                                                               prefectureCode: self.prefecturePickerView.selectedRow(inComponent: self.prefecturePickerViewSection),
                                                               successHandler: {
                                                                LoadingViewManager.shared.dismiss()
                                                                Utils.showAlert(context: self, title: "仮登録完了", message: "正式登録までお待ちください")},
                                                               errorHandler: {(apiError) in
                                                                LoadingViewManager.shared.dismiss()
                                                                Utils.showAlert(context: self, title: "エラー", message: "仮登録に失敗しました\nご不明な点はお問い合わせください")
                })
            } else {
                Utils.showAlert(context: self, title: "エラー", message: "仮登録に失敗しました\nご不明な点はお問い合わせください")
            }
        }
        
        saveAlert.addAction(cancelAction)
        saveAlert.addAction(saveAction)
        
        present(saveAlert, animated: true, completion: nil)
    }
    
    // MARK: Functions
    func finishedEditingPrefectureTextField(row: Int) {
        self.prefectureTextField.text = SpotsManager.shared.getPrefectureList()[row]

    }
    
    // MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField:UITextField){
        self.finishedEditingPrefectureTextField(row: prefecturePickerView.selectedRow(inComponent: prefecturePickerViewSection))
    }
    
    // MARK: UIPickerViewDelegate, UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return prefecturePickerViewSection + 1
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    numberOfRowsInComponent component: Int) -> Int {
        return SpotsManager.shared.getPrefectureCount()
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    titleForRow row: Int,
                    forComponent component: Int) -> String? {
        return SpotsManager.shared.getPrefectureList()[row]
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int) {
        self.finishedEditingPrefectureTextField(row: row)
    }
}
