//
//  LoginViewController.swift
//  BoulderLog
//
//  Created by 209crc on 2018/11/13.
//  Copyright © 2018年 209crc. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    let SegueLoginToTop: String = "LoginToTop"

    @IBOutlet var facebookLoginButton: FBSDKLoginButton!
    @IBOutlet var splashImageView: UIImageView!     // 途中でremoveFromSuperViewされるので、nilに注意
    
    // MARK : Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // todo : サイズに合わせて変える
//        splashImageView.image = UIImage.init(named: "LaunchImage")
        splashImageView.image = UIImage(named:"DefaultImage812")!
        
        facebookLoginButton.readPermissions = ["public_profile", "email"]
        facebookLoginButton.delegate = self
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // facebookログイン済み
        if ((FBSDKAccessToken.current()) != nil) {
            onFacebookLoggedIn()
        } else {
            splashImageView.removeFromSuperview()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // MARK : Action
    @IBAction func facebookLoginButtonTapped(sender:UIButton) {
        // memo : SDKの処理以外に何かしたくなったら書く
    }
    
    // MARK : FBSDKLoginButtonDelegate
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        onFacebookLoggedIn()
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    }
    
    // MARK : Function
    func onFacebookLoggedIn() {
        // facebookのデータ取得
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me",
                                                                 parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                Utils.showAlert(context: self, title: "エラー", message: "Facebookデータの取得に失敗しました")
            } else {
                let userProfile = result as! NSDictionary
                
                let user = User.init()
                user.name = userProfile.object(forKey: "first_name") as! String
                user.email = userProfile.object(forKey: "email") as! String
                user.fId = FBSDKAccessToken.current().userID
                
                APICaller.shared.postUsersLogin(user: user,
                                                successHandler: {user in
                                                    self.performSegue(withIdentifier: self.SegueLoginToTop, sender: nil)
                },
                                                errorHandler: {(apiError) in
                                                    Utils.showAlert(context: self, title: "エラー", message: "サービスへのログインに失敗しました")
                })
            }
        })
    }
}
